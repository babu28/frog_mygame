﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIRelatedScript : MonoBehaviour
{
    //Used for loading scenes
    public string sceneToLoad;

    public string currentScene;

    //Array to store created (prefab) levels
    public GameObject[] levels;

    //The blank object that covers the loaded level
    public GameObject blankScreen;

    //The current level that is being played
    public GameObject currentLevel;

    //The level that is currently stored
    public GameObject loadedLevel;

    //Bool stating whether game is mid transition or not
    private bool stageTransition;

    //Bool stating when a level is completed
    private bool completedLevel;

    //Bool stating if level is loaded into loadedLevel
    private bool loadedNewLevel;

    private float radius = 0;

    public bool completedLevell;

    // Start is called before the first frame update
    private void Start()
    {
        //Stating the current scene that is being played
        Scene loadedScene = SceneManager.GetActiveScene();
        currentScene = loadedScene.name;
    }

    // Update is called once per frame
    private void Update()
    {
        //Use specified functions when in specific scene
        if (currentScene == "UIStartScreen")
            LoadGameScene();

        if (currentScene == "UITest")
        {
            ForceScreenTransition();
            StartLevel();
        }
    }

    //Literally just starts the game
    public void LoadGameScene()
    {
        if (Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
        else if (Input.GetKeyDown("d"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
        else if (Input.GetKeyDown("a"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
        else if (Input.GetKeyDown("w"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
        else if (Input.GetKeyDown("s"))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }

    private IEnumerator ActuallyLoadGame()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneToLoad);
    }

    public void StageTransitionScreen()
    {
        completedLevell = false;
        //Teleport player to 0,0 in game perhaps
        //or have the position of the instantiate be where the player is

        //Teleport player back to "Start" object and thus spawn this stuff on that too

        //Places black screen in front of camera to block player from seeing levels load
        if (completedLevel == true)
        {
            loadedLevel = currentLevel;

            if (blankScreen != null)
                //Spawn the blank screen to hide what's actually happening in the background
                Instantiate(blankScreen, new Vector3(0, 0, 0), Quaternion.identity);

            Time.timeScale = 0;
            //Destroy the level that was being played
            Destroy(currentLevel);

            //If the next level wasn't loaded
            if (loadedNewLevel == false)
            {
                //Check what the previous level was and if it is equal to x, load the following level
                //Loading Level 2
                if (loadedLevel.name == levels[0].name)
                {
                    //Set loaded level to the next level
                    loadedLevel = levels[1];
                    //Actually load the level into the scene
                    Instantiate(loadedLevel, new Vector3(0, 0, 0), Quaternion.identity);
                    currentLevel = loadedLevel;
                }
                //Loading Level 3
                else if (loadedLevel.name == levels[1].name)
                {
                    //Set loaded level to the next level
                    loadedLevel = levels[2];
                    //Actually load the level into the scene
                    Instantiate(loadedLevel, new Vector3(0, 0, 0), Quaternion.identity);
                    currentLevel = loadedLevel;
                }
                //Loading Level 4
                else if (loadedLevel.name == levels[2].name)
                {
                    //Set loaded level to the next level
                    loadedLevel = levels[3];
                    //Actually load the level into the scene
                    Instantiate(loadedLevel, new Vector3(0, 0, 0), Quaternion.identity);
                    currentLevel = loadedLevel;
                }

                //The next level was successfully loaded and the player hasn't completed the level
                loadedNewLevel = true;
                completedLevel = false;
            }
        }
    }

    //Begin the level
    public void StartLevel()
    {
        //Find the darkness that covers the screen
        GameObject blankScreen = GameObject.FindGameObjectWithTag("ScreenTransition");

        //Space simulates starting the next level
        if (Input.GetButtonDown("Jump"))
        {
            //Find out what level is being played
            currentLevel = GameObject.FindGameObjectWithTag("Level");
            //Remove "clone" from the name
            currentLevel.name = currentLevel.name.Replace("(Clone)", "");
            //Rid of the darkness (in your heart)
            Destroy(blankScreen);
            Time.timeScale = 1;
        }
    }

    //This would be within the end of a level and would proc when the player gets there
    public void ForceScreenTransition()
    {
        if (completedLevell)
        {
            completedLevel = true;
            loadedNewLevel = false;
            StageTransitionScreen();
        }
    }
}