﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenThunder : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Vector3 Pos = Vector3.zero;
    //private void Awake()
    //{
    //    this.transform.parent = Camera.main.transform;
    //    this.transform.localPosition = new Vector3(0.2f, 0.8f, 10f);
    //}
    void Start()
    {
        this.transform.parent = Camera.main.transform;
        this.transform.localPosition = Pos;

        Destroy(gameObject, 0.7f);
    }
}
