﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour

   
{
    private float length, startpos;
    private float width, startposY;
    public GameObject cam;
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
     
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float temp = (cam.transform.position.x * (1 - parallaxEffect));
        float distance = (cam.transform.position.x * parallaxEffect);

       // float tempY = (cam.transform.position.y * (1 - parallaxEffect));
       // float distanceY = (cam.transform.position.y * parallaxEffect);
        transform.position = new Vector3(startpos + distance , transform.position.y, transform.position.z);;

        if (temp > startpos + length) startpos += (length * 2) - 0.5f;
        else if (temp < startpos - length) startpos -= length;
    }
}
