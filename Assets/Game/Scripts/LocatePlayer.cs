﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LocatePlayer : MonoBehaviour
{
    public GameObject player;

    public CinemachineVirtualCamera vcam;

    // Start is called before the first frame update
    void Start()
    {



    }

    private void Awake()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
        player = GameObject.FindGameObjectWithTag("Player");


    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");


        if (player != null)
        {
            vcam.Follow = player.transform;
            vcam.LookAt = player.transform;

        }
    }


   
}
