﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToControls : MonoBehaviour
{

    //public GameObject jumpEffect;
    //public GameObject slashText;
    //public GameObject slashStick;
    //public Animator m_animator;
    [SerializeField]
    float Timer = 0f;
    [SerializeField]
    Vector3 pos;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DoAnimations());
    }

    IEnumerator DoAnimations()
    {
        LeanTween.move(this.gameObject, pos, Timer).setEase(LeanTweenType.easeInQuad);
        yield return new WaitForSeconds(Timer);
        LeanTween.alpha(this.gameObject, 0, 1.5f);
    }
    // Update is called once per frame
    void Update()
    {


    }

    
}
