﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameCreation : MonoBehaviour
{

    public string[] characters = new string[31]
    {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"," ", "Ü", "&", "α", "Ω" };

    public string[] playerCharacters;
    public Text charText;



    public Text playerNameText;
    public int charSelect;

    public string charDisplay;

    public int charNumber;
    public string playerName;

    public GameObject loadAndSave;
    public GameObject highScoreDisplay;

    public bool creatingName;

    public Text[] blankChars;
    // Start is called before the first frame update
    void Start()
    {
        charNumber = 0;
        creatingName = loadAndSave.GetComponent<LoadAndSave>().highScores.newHighScoreAchieved;
    }

    // Update is called once per frame
    void Update()
    {
        if (creatingName == true)
        {
            if (creatingName)
            {
                CharacterChoice();
                CharacterSelect();

            }
        }
        else if (creatingName == false)
        {
            StartCoroutine(LoadScores());
            blankChars[0].gameObject.SetActive(false);
            blankChars[1].gameObject.SetActive(false);
            blankChars[2].gameObject.SetActive(false);
            blankChars[3].gameObject.SetActive(false);
            blankChars[4].gameObject.SetActive(false);
        }
        charText.text = charDisplay;

        playerName = (playerCharacters[0] + playerCharacters[1] + playerCharacters[2] + playerCharacters[3] + playerCharacters[4]);
        playerNameText.text = playerName;
    }



    public void CharacterChoice()
    {

        if (charSelect < 0)
            charSelect = 30;
        else if (charSelect > 30)
            charSelect = 0;

        if (Input.GetKeyDown("w"))
        {
            charSelect += 1;

        }
        else if (Input.GetKeyDown("s"))
        {
            charSelect -= 1;

        }
        charDisplay = characters[charSelect];
    }

    public void CharacterSelect()
    {
        if (Input.GetButtonDown("Jump") && charNumber == 0)
        {
            playerCharacters[0] = charDisplay;
            charNumber += 1;
            blankChars[0].gameObject.SetActive(false);
        }
        else if (Input.GetButtonDown("Jump") && charNumber == 1)
        {
            playerCharacters[1] = charDisplay;
            charNumber += 1;
            blankChars[1].gameObject.SetActive(false);
        }
        else if (Input.GetButtonDown("Jump") && charNumber == 2)
        {
            playerCharacters[2] = charDisplay;
            charNumber += 1;
            blankChars[2].gameObject.SetActive(false);
        }
        else if (Input.GetButtonDown("Jump") && charNumber == 3)
        {
            playerCharacters[3] = charDisplay;
            charNumber += 1;
            blankChars[3].gameObject.SetActive(false);
        }
        else if (Input.GetButtonDown("Jump") && charNumber == 4)
        {
            playerCharacters[4] = charDisplay;
            charNumber += 1;
            blankChars[4].gameObject.SetActive(false);
            StartCoroutine(LoadScores());
            /*loadAndSave.GetComponent<LoadAndSave>().SaveName();
            charText.gameObject.SetActive(false);
            playerNameText.gameObject.SetActive(false);
            creatingName = false;
            highScoreDisplay.GetComponent<HighScoreDisplay>().DisplayHighScore();
            */

        }

    }

    IEnumerator LoadScores()
    {
        yield return new WaitForSeconds(0.1f);
        loadAndSave.GetComponent<LoadAndSave>().highScores.newHighScoreAchieved = false;
        charText.gameObject.SetActive(false);
        playerNameText.gameObject.SetActive(false);
        creatingName = false;
        yield return new WaitForSeconds(0.1f);
        
        loadAndSave.GetComponent<LoadAndSave>().SaveName();
        
        highScoreDisplay.GetComponent<HighScoreDisplay>().DisplayHighScore();

    }
}
