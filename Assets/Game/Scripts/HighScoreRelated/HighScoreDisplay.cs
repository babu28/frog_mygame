﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HighScoreDisplay : MonoBehaviour
{
    public LoadAndSave loadAndSave;
    public GameObject blank;

    public Text[] names;
    public Text[] scores;

    private Color ogColoursAlpha;
    private Color disappearColoursAlpha;

    public bool alphaIsNotZero;
    public bool showingHighScores;

    /*   // Update is called once per frame
       void Update()
       {
           //This is to simulate that the player inserted their name and submits it
           if (Input.GetKeyDown("space"))
               DisplayHighScore();
       }*/

    //Although this is messy, it simply loads the arrays from the Save File

    public void Start()
    {
        alphaIsNotZero = false;
        showingHighScores = false;

        names[0].GetComponent<CanvasRenderer>().SetAlpha(0);
        names[1].GetComponent<CanvasRenderer>().SetAlpha(0);
        names[2].GetComponent<CanvasRenderer>().SetAlpha(0);
        names[3].GetComponent<CanvasRenderer>().SetAlpha(0);
        names[4].GetComponent<CanvasRenderer>().SetAlpha(0);

        scores[0].GetComponent<CanvasRenderer>().SetAlpha(0);
        scores[1].GetComponent<CanvasRenderer>().SetAlpha(0);
        scores[2].GetComponent<CanvasRenderer>().SetAlpha(0);
        scores[3].GetComponent<CanvasRenderer>().SetAlpha(0);
        scores[4].GetComponent<CanvasRenderer>().SetAlpha(0);
    }

    public void Update()
    {
        if (alphaIsNotZero)
        {
            for (int i = 0; i < names.Length; i++)
            {
                names[i].CrossFadeAlpha(1, 2, false);
                scores[i].CrossFadeAlpha(1, 2, false);
            }
        }

        if (showingHighScores && (Input.GetButtonDown("Jump")))
            SceneManager.LoadScene("UIStartScreen");
    }

    public void DisplayHighScore()
    {
        alphaIsNotZero = true;
        showingHighScores = true;

        names[0].text = loadAndSave.highScores.savedNames[0];
        names[1].text = loadAndSave.highScores.savedNames[1];
        names[2].text = loadAndSave.highScores.savedNames[2];
        names[3].text = loadAndSave.highScores.savedNames[3];
        names[4].text = loadAndSave.highScores.savedNames[4];

        scores[0].text = "" + loadAndSave.highScores.savedHighScores[0];
        scores[1].text = "" + loadAndSave.highScores.savedHighScores[1];
        scores[2].text = "" + loadAndSave.highScores.savedHighScores[2];
        scores[3].text = "" + loadAndSave.highScores.savedHighScores[3];
        scores[4].text = "" + loadAndSave.highScores.savedHighScores[4];

        //Destroy(blank);
    }
}