﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    //Player's current score/kills
    public int score;
    public Text scoreText;


    public int enemyTotal;

    public int scoreToAdd;
    //Used to store the difference between the amount of enemies in the level and the enemyTotal
    public int differenceInEnemies;

    // Start is called before the first frame update
    void Start()
    {
        SetNewEnemyTotal();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = " Kills: " + score;

        //if (Input.GetKeyDown("q"))
        //{
        //    AddToScore();
        //}

        KilledAnEnemy();
    }

    public void AddToScore()
    {
        score += 1;
    }

    public void KilledAnEnemy()
    {
        //Debug.Log("The Difference in Enemies" + differenceInEnemies);
        //Debug.Log("Score to Add" + scoreToAdd);

        //differenceInEnemies = GameObject.FindGameObjectsWithTag("enemy").Length - enemyTotal;

        //scoreToAdd = differenceInEnemies * -1;


        if (enemyTotal != GameObject.FindGameObjectsWithTag("enemy").Length)
        {
            AddToScore();
            SetNewEnemyTotal();
        }
        
    }

    public void SetNewEnemyTotal()
    {
        enemyTotal = GameObject.FindGameObjectsWithTag("enemy").Length;
    }
}
