﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HighScores
{
    public string[] savedNames = new string[5];
    public int[] savedHighScores = new int[5];

    public bool newHighScoreAchieved;
}
