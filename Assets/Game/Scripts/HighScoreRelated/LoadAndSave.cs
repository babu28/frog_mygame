﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadAndSave : MonoBehaviour
{
    public HighScores highScores = new HighScores();

    public GameObject namecreation;

    public GameObject scoreManger;

    public bool died;

    //The text that the player inputs for their name
    //public Text inputField;

    //Used later to save the inputed name
    public string newName;

    private void Start()
    {
        //Load scores
        LoadSaveFile();
        died = false;
        //Sort the array from least to greatest score as well as connect the names with the scores
        if (SceneManager.GetActiveScene().name == "UITest")
        {
            Array.Sort(highScores.savedHighScores, highScores.savedNames);
            highScores.newHighScoreAchieved = false;
        }

    }

    // Update is called once per frame
    private void Update()
    {
        //If in the scene UITest, run the SaveScores function
        if (SceneManager.GetActiveScene().name == "UITest")
            SaveScores();

        //If in the scene HighScoreScreen, run the InputName function
        else if (SceneManager.GetActiveScene().name == "HighScoreScreen" && highScores.newHighScoreAchieved)
        {
            InputName();
        }
    }

    public void SaveScores()
    {
        //If reached end of game or died
        if (died)
        {
            for (int i = 0; i < highScores.savedHighScores.Length; i++)
            {
                //If the player's score is greater than any of the saved scores
                if (scoreManger.GetComponent<ScoreManager>().score > highScores.savedHighScores[i])
                {
                    Debug.Log("Good job mate, yeh beat a score!");
                    //Replace the lowest score with the player's new score
                    highScores.savedHighScores[0] = scoreManger.GetComponent<ScoreManager>().score;
                    highScores.newHighScoreAchieved = true;
                    //Save the new score
                    RewriteSaveFile();

                    //Load the next scene
                    SceneManager.LoadScene("HighScoreScreen");
                    Debug.Log("Saved Scores.");
                }
                //If the player's score is less than all the saved scores
                else if (scoreManger.GetComponent<ScoreManager>().score < highScores.savedHighScores[0])
                SceneManager.LoadScene("HighScoreScreen");
            }
        }
    }

    //Function for inserting player name and saving name
    public void InputName()
    {
        //Replace the lowest score name with the new inputed name
        newName = namecreation.GetComponent<NameCreation>().playerName;
        highScores.savedNames[0] = newName;
    }

    public void SaveName()
    {
        //Re-sort the array now that the player's name and score have been inputed
        Array.Sort(highScores.savedHighScores, highScores.savedNames);

        //Save the newly sorted information/array
        RewriteSaveFile();
    }

    public void LoadSaveFile()
    {
        highScores = JsonTools.DeserializeObject<HighScores>(Application.dataPath + "/Game" + "/SaveFiles" + "/" + "SaveFile");
        // Debug.Log("Loaded Scores.");
    }

    public void RewriteSaveFile()
    {
        JsonTools.SaveSerializedObject(highScores, Application.dataPath + "/Game" + "/SaveFiles", "SaveFile");
        // Debug.Log("Saved Scores.");
    }
}