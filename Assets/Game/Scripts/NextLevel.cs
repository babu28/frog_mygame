﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour
{
    public GameObject uIManager;

    // Start is called before the first frame update
    void Start()
    {



    }

    private void Awake()
    {
        uIManager = GameObject.FindGameObjectWithTag("UIRelated");


    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            uIManager.GetComponent<UIRelatedScript>().completedLevell = true;

    }
}
