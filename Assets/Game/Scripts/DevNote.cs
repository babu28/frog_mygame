﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DevNote : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("START MENU CONTROLS: " +
            "SPACEBAR = Start Game" +
            "GAME CONTROLS:" +
            "A KEY = Finish a Level" +
            "SPACEBAR = Start Next Level" +
            "Q KEY = Add to kill count" +
            "S KEY = Finished last level or died");
    }


}
