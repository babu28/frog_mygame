﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGlowIntensity : MonoBehaviour
{
    public bool isAppear;
    public bool isDisappear;

    private bool m_isKillStreek;

    Material glowMaterial;
    float fadeVal = 1.0f;
    private float m_killStreekFade = 0.0f;
    void Start()
    {
        glowMaterial = GetComponent<SpriteRenderer>().material;
        m_killStreekFade = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDisappear == true)
        {
            Disappear();
            return;
        }
        if (isAppear == true)
        {
            Appear();
            return;
        }


        if (m_isKillStreek == true)
        {
            DecreaseFade();
        }
    }


    void Appear()
    {
        //fadeVal = 0.0f;
        fadeVal += 0.10f;
        //fadeVal += Time.deltaTime;
        if (fadeVal >= 1.0f)
        {
            fadeVal = 1.0f;
            isAppear = false;
            KillStreekFade();
        }

        glowMaterial.SetFloat("_Fade", fadeVal);

    }

    void Disappear()
    {
        //fadeVal = 1.0f;
        fadeVal -= 0.10f;
       // fadeVal -= Time.deltaTime;
        if (fadeVal <= 0.0f)
        {
            fadeVal = 0.0f;
            isDisappear = false;
        }

        glowMaterial.SetFloat("_Fade", fadeVal);

    }

    public void KillStreekFade()
    {
        m_killStreekFade -= 0.3f;
        m_killStreekFade = Mathf.Clamp(m_killStreekFade, 0.4f, 1.0f);
        glowMaterial.SetFloat("_Fade", m_killStreekFade);
        m_isKillStreek = true;
    }

    private void DecreaseFade()
    {
        m_killStreekFade += 0.0025f;
        if (m_killStreekFade >= 1.0f)
        {
            m_killStreekFade = 1.0f;
            m_isKillStreek = false;
        }

        glowMaterial.SetFloat("_Fade", m_killStreekFade);
    }
}
