﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform target;

    [SerializeField]
    Vector3 offset;
    [SerializeField]
    float smoothTime = 0.3f;
 
    //float tempSmoothTime = 0.0f;
//    [SerializeField]
//  Vector3 size = Vector3.zero;

    private void Start()
    {
        //tempSmoothTime = smoothTime;
    }
    private void FixedUpdate()
    {


        transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.deltaTime * smoothTime);

        //Collider2D[] colliders = Physics2D.OverlapBoxAll(target.position, size, 0);
        //for (int i = 0; i < colliders.Length; i++)
        //{
        //    if (colliders[i].gameObject != gameObject)
        //    {
        //        tempSmoothTime = smoothTime;
        //    }
        //    else
        //    {
        //        smoothTime = 0.000001f;
        //    }
              
        //}
    }

    private void OnDrawGizmos()
    {
       // Gizmos.DrawCube(transform.position, size);
    }
}
