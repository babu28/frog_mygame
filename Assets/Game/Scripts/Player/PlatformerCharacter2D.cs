using System.Collections;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField]
        private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.

        [SerializeField]
        private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.

        [SerializeField]
        private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;                                 // A position marking where to check if the player is grounded.
        private const float k_GroundedRadius = 0.02f;                    // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;                                         // Whether or not the player is grounded.
        private Animator m_Anim;                                         // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;                               // For determining which way the player is currently facing.

        public GameObject grassEffect;
        private Vector2 tempVelocity;

        private float moveSpeed = 1.0f;
        private float timer = 0.0f;
        private bool isinTrigger = false;
        private GameObject coll;
        private float radius = 0;

        [SerializeField]
        private float waitforAttackTime = 0.75f;

        [SerializeField]
        private float timeToAttack = 2.0f;

        [SerializeField]
        private GameObject swordSwing;

        [SerializeField]
        private GameObject swordSlice;

        [SerializeField]
        private GameObject swordParticles;

        [SerializeField]
        private float playerOffsetPos = 10.0f;

        [SerializeField]
        private GameObject teleport;

        [SerializeField]
        private GameObject thunder;

        private bool isCoroutineRunning = false;

        private bool isInput = false;

        public GameObject soundManager;

        public GameObject soundManagerThunder;

        public GameObject soundManagerLongSlice;

        private PlayerGlowIntensity glowIntensity;

        [SerializeField]
        public GameObject longSlice;

        private void Awake()
        {
            soundManager = GameObject.FindGameObjectWithTag("SoundManager");

            soundManagerThunder = GameObject.FindGameObjectWithTag("SoundManagerr");

            soundManagerLongSlice = GameObject.FindGameObjectWithTag("SoundManagerr");

            glowIntensity = GetComponent<PlayerGlowIntensity>();

            m_GroundCheck = transform.Find("GroundCheck");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            if (isinTrigger && isInput != true)
            {
               float x = Input.GetAxis("Horizontal");
                float y = Input.GetAxis("Vertical");

                m_Anim.SetBool("camJump", false);
                timer += Time.deltaTime;
                //Ghost.makeGhost = true;
                if (timer < timeToAttack)
                {
                    if ((x != 0 || y != 0))
                    {
                        isInput = true;
                        if (y > 0 && x == 0)
                        {
                            glowIntensity.isDisappear = true;
                            Instantiate(teleport, this.transform.position, Quaternion.Euler(0, 0, 0));
                            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            // gameObject.transform.position = new Vector2(coll.transform.position.x, coll.transform.position.y - 2);
                            // transform.position = new Vector2(coll.transform.position.x, coll.transform.position.y + radius + playerOffsetPos);

                            StartCoroutine(waitforAction("up"));

                            soundManagerLongSlice.GetComponent<SoundsManagerr>().PlaySwordLine();
                            Instantiate(longSlice, coll.transform.position, Quaternion.Euler(0, 0, -90));
                        }
                        else if (x > 0 && y == 0)
                        {
                            glowIntensity.isDisappear = true;

                            Instantiate(teleport, this.transform.position, Quaternion.Euler(0, 0, 0));
                            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            //gameObject.transform.position = new Vector2(coll.transform.position.x - 2, coll.transform.position.y);
                            // transform.position = new Vector2(coll.transform.position.x + radius + playerOffsetPos, coll.transform.position.y);

                            StartCoroutine(waitforAction("right"));

                            soundManagerLongSlice.GetComponent<SoundsManagerr>().PlaySwordLine();
                            Instantiate(longSlice, coll.transform.position, Quaternion.Euler(0, 0, 0));
                        }
                        else if (y < 0 && x == 0)
                        {
                            glowIntensity.isDisappear = true;

                            Instantiate(teleport, this.transform.position, Quaternion.Euler(0, 0, 0));
                            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            //  gameObject.transform.position = new Vector2(coll.transform.position.x, coll.transform.position.y + 2);
                            // transform.position = new Vector2(coll.transform.position.x - radius, coll.transform.position.y - radius - playerOffsetPos);

                            StartCoroutine(waitforAction("down"));

                            soundManagerLongSlice.GetComponent<SoundsManagerr>().PlaySwordLine();
                            Instantiate(longSlice, coll.transform.position, Quaternion.Euler(0, 0, 90));
                        }
                        else if (y > 0 && x > 0)
                        {
                            glowIntensity.isDisappear = true;

                            Instantiate(teleport, this.transform.position, Quaternion.Euler(0, 0, 0));
                            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            // gameObject.transform.position = new Vector2(coll.transform.position.x - 2, coll.transform.position.y - 2);
                            // transform.position = new Vector2(coll.transform.position.x + radius  , coll.transform.position.y + radius );

                            StartCoroutine(waitforAction("upright"));

                            soundManagerLongSlice.GetComponent<SoundsManagerr>().PlaySwordLine();
                            Instantiate(longSlice, coll.transform.position, Quaternion.Euler(0, 0, 45));
                        }
                        else if (y < 0 && x > 0)
                        {
                            glowIntensity.isDisappear = true;

                            Instantiate(teleport, this.transform.position, Quaternion.Euler(0, 0, 0));
                            // gameObject.GetComponent<SpriteRenderer>().enabled = false;
                            //  gameObject.transform.position = new Vector2(coll.transform.position.x - 2, coll.transform.position.y + 2);
                            //transform.position = new Vector2(coll.transform.position.x + radius +  , coll.transform.position.y - radius );

                            StartCoroutine(waitforAction("dowright"));

                            soundManagerLongSlice.GetComponent<SoundsManagerr>().PlaySwordLine();
                            Instantiate(longSlice, coll.transform.position, Quaternion.Euler(0, 0, -45));
                        }
                        isInput = true;
                    }
                }
                else
                {
                    m_Rigidbody2D.gravityScale = 3;
                    moveSpeed = 1;
                    timer = 0;
                    isinTrigger = false;
                    coll.GetComponent<CircleCollider2D>().enabled = false;
                    StopCoroutine(GhostCouroutine());
                    isCoroutineRunning = false;
                    isInput = false;
                }
            }
            else
            {
                m_Anim.SetBool("camJump", true);
            }
        }

        private void FixedUpdate()
        {
            m_Grounded = false;

            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                    m_Grounded = true;
            }

            m_Anim.SetBool("Ground", m_Grounded);

            if (m_Grounded)
            {
                grassEffect.SetActive(true);
            }
            else
            {
                grassEffect.SetActive(false);
            }
        }

        private IEnumerator waitforAction(string input)
        {
            yield return new WaitForSeconds(waitforAttackTime);
            Camera.main.GetComponent<CameraShake>().startShake();

            Ghost.makeGhost = true;
            if (isCoroutineRunning == true)
            {
                StopCoroutine(GhostCouroutine());
            }
            StartCoroutine(GhostCouroutine());

            switch (input)
            {
                case "up":
                    //gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    glowIntensity.isAppear = true;

                    Instantiate(swordSlice, coll.transform.position, Quaternion.Euler(0, 0, 45));
                    coll.GetComponent<Animator>().SetTrigger("upDeath");
                    Instantiate(teleport, transform.position, Quaternion.Euler(0, 0, 0));
                    transform.position = new Vector2(coll.transform.position.x, coll.transform.position.y + radius + playerOffsetPos);
                    m_Anim.SetTrigger("Slice");
                    Instantiate(swordSwing, new Vector2(coll.transform.position.x, coll.transform.position.y + radius + playerOffsetPos), Quaternion.Euler(0, 0, -125f));
                    Instantiate(swordParticles, new Vector2(coll.transform.position.x, coll.transform.position.y - 2), Quaternion.Euler(0, 0, 0));

                    soundManager.GetComponent<SoundsManagerr>().PlaySlashSound();
                    soundManagerThunder.GetComponent<SoundsManagerr>().PlayThunderSound();
                    Destroy(coll.gameObject, 2.0f);
                    Instantiate(thunder, coll.transform.position, Quaternion.Euler(0, 0, 0));

                    break;

                case "right":
                    //gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    glowIntensity.isAppear = true;

                    Instantiate(swordSlice, coll.transform.position, Quaternion.Euler(0, 0, -45));
                    coll.GetComponent<Animator>().SetTrigger("centerDeath");
                    Instantiate(teleport, transform.position, Quaternion.Euler(0, 0, 0));
                    transform.position = new Vector2(coll.transform.position.x + radius + playerOffsetPos, coll.transform.position.y);
                    m_Anim.SetTrigger("Slice");
                    Instantiate(swordSwing, new Vector2(coll.transform.position.x + radius + playerOffsetPos, coll.transform.position.y), Quaternion.Euler(0, 0, -125f));
                    Instantiate(swordParticles, coll.transform.position, Quaternion.Euler(0, 0, 0));

                    soundManager.GetComponent<SoundsManagerr>().PlaySlashSound();
                    soundManagerThunder.GetComponent<SoundsManagerr>().PlayThunderSound();

                    Destroy(coll.gameObject, 2.0f);
                    Instantiate(thunder, coll.transform.position, Quaternion.Euler(0, 0, 90));

                    break;

                case "down":
                    //gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    glowIntensity.isAppear = true;

                    Instantiate(swordSlice, coll.transform.position, Quaternion.Euler(0, 0, 225));
                    coll.GetComponent<Animator>().SetTrigger("upDeath");
                    Instantiate(teleport, transform.position, Quaternion.Euler(0, 0, 0));
                    transform.position = new Vector2(coll.transform.position.x - radius, coll.transform.position.y - radius - playerOffsetPos);
                    m_Anim.SetTrigger("Slice");
                    Instantiate(swordSwing, new Vector2(coll.transform.position.x - radius, coll.transform.position.y - radius - playerOffsetPos), Quaternion.Euler(0, 0, -125f));
                    Instantiate(swordParticles, coll.transform.position, Quaternion.Euler(0, 0, 0));

                    soundManager.GetComponent<SoundsManagerr>().PlaySlashSound();

                    soundManagerThunder.GetComponent<SoundsManagerr>().PlayThunderSound();

                    Destroy(coll.gameObject, 2.0f);
                    Instantiate(thunder, coll.transform.position, Quaternion.Euler(0, 0, -180));

                    break;

                case "upright":
                    //gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    glowIntensity.isAppear = true;

                    Instantiate(swordSlice, coll.transform.position, Quaternion.Euler(0, 0, 0));
                    coll.GetComponent<Animator>().SetTrigger("rightDeath");
                    Instantiate(teleport, transform.position, Quaternion.Euler(0, 0, 0));
                    transform.position = new Vector2(coll.transform.position.x + radius + (playerOffsetPos - 2), coll.transform.position.y + radius + (playerOffsetPos - 2));
                    m_Anim.SetTrigger("Slice");
                    Instantiate(swordSwing, new Vector2(coll.transform.position.x + radius + playerOffsetPos, coll.transform.position.y + radius + playerOffsetPos), Quaternion.Euler(0, 0, -125f));
                    Instantiate(swordParticles, coll.transform.position, Quaternion.Euler(0, 0, 0));

                    soundManager.GetComponent<SoundsManagerr>().PlaySlashSound();
                    soundManagerThunder.GetComponent<SoundsManagerr>().PlayThunderSound();

                    Destroy(coll.gameObject, 2.0f);
                    Instantiate(thunder, coll.transform.position, Quaternion.Euler(0, 0, -45));

                    break;

                case "dowright":
                    //gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    glowIntensity.isAppear = true;

                    Instantiate(swordSlice, coll.transform.position, Quaternion.Euler(0, 0, -90));
                    coll.GetComponent<Animator>().SetTrigger("leftDeath");
                    Instantiate(teleport, transform.position, Quaternion.Euler(0, 0, 0));
                    transform.position = new Vector2(coll.transform.position.x + radius + (playerOffsetPos - 2), coll.transform.position.y + radius - (playerOffsetPos - 2));
                    m_Anim.SetTrigger("Slice");
                    Instantiate(swordSwing, new Vector2(coll.transform.position.x + radius + playerOffsetPos, coll.transform.position.y + radius - playerOffsetPos), Quaternion.Euler(0, 0, -125f));
                    Instantiate(swordParticles, coll.transform.position, Quaternion.Euler(0, 0, 0));

                    soundManager.GetComponent<SoundsManagerr>().PlaySlashSound();
                    soundManagerThunder.GetComponent<SoundsManagerr>().PlayThunderSound();

                    Destroy(coll.gameObject, 2.0f);
                    Instantiate(thunder, coll.transform.position, Quaternion.Euler(0, 0, 45));

                    break;
            }
        }

        public void Move(float move, bool crouch, bool jump)
        {
            move = moveSpeed;

            m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.y);

            if (move > 0 && !m_FacingRight)
            {
                Flip();
            }
            else if (move < 0 && m_FacingRight)
            {
                Flip();
            }

            if (m_Grounded && jump && m_Anim.GetBool("Ground"))
            {
                m_Grounded = false;
                m_Anim.SetBool("Ground", false);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            }
        }

        private void Flip()
        {
            m_FacingRight = !m_FacingRight;

            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Ghost.makeGhost = false;

            tempVelocity = m_Rigidbody2D.velocity;
            if (collision.tag == "enemy")
            {
                isInput = false;

                radius = collision.gameObject.GetComponent<CircleCollider2D>().radius;
                isinTrigger = true;
                coll = collision.gameObject;
                moveSpeed = 0.08f;
                m_Rigidbody2D.velocity = Vector2.zero;
                m_Rigidbody2D.gravityScale = 0;
                //Time.timeScale = 0.01f;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            m_Rigidbody2D.velocity = tempVelocity;
            m_Rigidbody2D.gravityScale = 3;
            moveSpeed = 1;
            //Time.timeScale = 1.0f;
            timer = 0;
            isinTrigger = false;
            StopCoroutine(GhostCouroutine());
            isCoroutineRunning = false;
            isInput = true;
        }

        private IEnumerator GhostCouroutine()
        {
            isCoroutineRunning = true;
            yield return new WaitForSeconds(5f);
            Ghost.makeGhost = false;
            isCoroutineRunning = false;
        }
    }
}