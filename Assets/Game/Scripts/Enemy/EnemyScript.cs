﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField]
    GameObject player;

    [SerializeField]
    float distToPlayer;

    [SerializeField]
    GameObject circle;

    bool hasCircle = false;

    GameObject target;

    float dist;

    private void Update()
    {
        dist = Vector2.Distance(gameObject.transform.position, player.transform.position);
        if (dist < distToPlayer && hasCircle != true)
        {
            target = Instantiate(circle, transform.position, transform.rotation);
            target.transform.parent = transform;
            hasCircle = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            Destroy(target , 1.0f);
        }

    }
}
