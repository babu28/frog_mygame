﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeNextLevel : MonoBehaviour
{

    public Text fakeText;

    public bool alphaIsNotZero;

    // Start is called before the first frame update
    void Start()
    {
        alphaIsNotZero = false;

        fakeText.GetComponent<CanvasRenderer>().SetAlpha(0);

    }

    // Update is called once per frame
    void Update()
    {
        if (alphaIsNotZero)
        {
            fakeText.CrossFadeAlpha(1, 2, false);
        }
        else if (!alphaIsNotZero)
        {

            fakeText.CrossFadeAlpha(0, 2, false);
        }
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(FakeLoading());
        }
    }

    IEnumerator FakeLoading()
    {

        alphaIsNotZero = true;
        yield return new WaitForSeconds(5);
        alphaIsNotZero = false;

    }

}
