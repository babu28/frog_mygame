﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundsManagerr : MonoBehaviour
{
    [Header("Values are hard coded strings need to change in code for sound to play on scenes")]

    private AudioSource audioSource;

    public AudioClip titleScreenBGMStart;
    public AudioClip titleScreenBGMRepeat;

    public AudioClip gameplayBGMStart;
    public AudioClip gameplayBGMRepeat;

    public AudioClip runningSound;

    public AudioClip swordFleshSlice1;
    public AudioClip swordFleshSlice2;
    public AudioClip swordFleshSlice3;

    public AudioClip[] swordSlices;
    public AudioClip[] swordLine;
    public AudioClip[] thunder;

    public bool startBGM;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene().name == "UIStartScreen")
        {
            audioSource.clip = titleScreenBGMStart;
        }

        if (SceneManager.GetActiveScene().name == "UITest")
        {
            audioSource.clip = gameplayBGMStart;
        }
        audioSource.Play();
    }

    public void Update()
    {
        if (SceneManager.GetActiveScene().name == "UIStartScreen")
        {
            if (!audioSource.isPlaying)
            {
                audioSource.clip = titleScreenBGMRepeat;
                audioSource.Play();
            }
        }

        if (SceneManager.GetActiveScene().name == "UITest")
        {
            if (!audioSource.isPlaying)
            {
                audioSource.clip = gameplayBGMStart;
                audioSource.Play();
            }
        }
    }

    public void PlaySlashSound()
    {
        var swordSoundChoice = Random.Range(0, swordSlices.Length);

        audioSource.PlayOneShot(swordSlices[swordSoundChoice], 0.5f);
    }

    public void PlayThunderSound()
    {
        var thunderSoundChoice = Random.Range(0, thunder.Length);

        audioSource.PlayOneShot(thunder[thunderSoundChoice], 0.7f);
    }

    public void PlaySwordLine()
    {
        var swordLineSoundChoice = Random.Range(0, swordLine.Length);

        audioSource.PlayOneShot(swordLine[swordLineSoundChoice], 0.5f);
    }
}