﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TitleScreenLoader : MonoBehaviour
{
    public Renderer rendBlank;
    public Renderer rendFlash;

    public Color altColor = Color.red;
    public Color regColor;

    // Transforms to act as start and end markers for the journey.
    public Transform startMarkerForImageA;

    public Transform startMarkerForImageB;
    public Transform endMarkerForImageA;

    // Movement speed in units per second.
    public float speedB;

    public float speedA;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLengthA;

    private float journeyLengthB;

    public GameObject trees;
    public GameObject foregroundTrees;
    public Renderer trueTitle;

    private bool startTreesNMountainMovement;

    public Text title;
    public Renderer sword;
    public Renderer frug;
    public Renderer hill;
    public Renderer slashKeys;

    public bool slideHasBeenCalled;

    public bool moveTrees;

    public bool flashClick;

    public GameObject loadAndSave;

    public GameObject uITing;

    public Text start;

    // Start is called before the first frame update
    private void Start()
    {
        loadAndSave.GetComponent<LoadAndSave>().highScores.newHighScoreAchieved = false;
        loadAndSave.GetComponent<LoadAndSave>().RewriteSaveFile();

        rendFlash.material.color = new Color(0.00f, 0.00f, 0.00f, 0.00f);

        sword.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 0.00f);
        frug.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 0.00f);
        hill.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 0.00f);
        trueTitle.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 0.00f);
        slashKeys.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 0.00f);

        title.GetComponent<Text>().color = new Color(title.GetComponent<Text>().color.r, title.GetComponent<Text>().color.g, title.GetComponent<Text>().color.b, 0.00f);
        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLengthA = Vector3.Distance(startMarkerForImageA.position, endMarkerForImageA.position);
        journeyLengthB = Vector3.Distance(startMarkerForImageB.position, endMarkerForImageA.position);

        start.GetComponent<CanvasRenderer>().SetAlpha(0);

        StartCoroutine(BlankScreenDisappear());
    }

    // Update is called once per frame
    private void Update()
    {
        //Debug.Log(rendFlash.material.color);
        if (startTreesNMountainMovement)
            StartCoroutine(SlideMountatainsAndTrees());

        if (moveTrees)
            TreesAnimation();

        if (flashClick)
            StartCoroutine(flashClickTing());
    }

    public void TreesAnimation()
    {
        // Distance moved equals elapsed time times speed..
        float distCoveredA = (Time.time - startTime) * speedA;
        float distCoveredB = (Time.time - startTime) * speedB;
        // Fraction of journey completed equals current distance divided by total distance.
        float fractionOfJourneyA = distCoveredA / journeyLengthA;
        float fractionOfJourneyB = distCoveredB / journeyLengthB;
        // Set our position as a fraction of the distance between the markers.
        trees.transform.position = Vector3.Lerp(startMarkerForImageA.position, endMarkerForImageA.position, fractionOfJourneyA);

        foregroundTrees.transform.position = Vector3.Lerp(startMarkerForImageB.position, endMarkerForImageA.position, fractionOfJourneyB);
    }

    private IEnumerator flashClickTing()
    {
        flashClick = false;

        start.GetComponent<CanvasRenderer>().SetAlpha(1);
        yield return new WaitForSeconds(1f);
        start.GetComponent<CanvasRenderer>().SetAlpha(0);
        yield return new WaitForSeconds(0.5f);
        start.GetComponent<CanvasRenderer>().SetAlpha(1);
        yield return new WaitForSeconds(1f);
        start.GetComponent<CanvasRenderer>().SetAlpha(0);
        yield return new WaitForSeconds(0.5f);
        start.GetComponent<CanvasRenderer>().SetAlpha(1);
        yield return new WaitForSeconds(1f);
        start.GetComponent<CanvasRenderer>().SetAlpha(0);
        yield return new WaitForSeconds(0.5f);
        start.GetComponent<CanvasRenderer>().SetAlpha(1);
        yield return new WaitForSeconds(1f);
        start.GetComponent<CanvasRenderer>().SetAlpha(0);
        yield return new WaitForSeconds(0.5f);
        start.GetComponent<CanvasRenderer>().SetAlpha(1);
        yield return new WaitForSeconds(1f);
        start.GetComponent<CanvasRenderer>().SetAlpha(0);
        yield return new WaitForSeconds(0.5f);

        flashClick = true;
    }

    private IEnumerator SlideMountatainsAndTrees()
    {
        startTreesNMountainMovement = false;
        moveTrees = true;
        yield return new WaitForSeconds(2f);

        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);

        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        yield return new WaitForSeconds(0.05f);
        rendFlash.material.color += new Color(0.08f, 0.08f, 0.08f, 0.08f);
        sword.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 1.00f);
        frug.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 1.00f);
        hill.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 1.00f);
        title.GetComponent<Text>().color = new Color(title.GetComponent<Text>().color.r, title.GetComponent<Text>().color.g, title.GetComponent<Text>().color.b, 1.00f);
        trueTitle.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 1.00f);
        slashKeys.material.color = new Color(title.material.color.r, title.material.color.g, title.material.color.b, 1.00f);

        Debug.Log(title.GetComponent<Text>().color);
        StartCoroutine(WhiteScreenDisappear());
    }

    private IEnumerator WhiteScreenDisappear()
    {
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);

        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendFlash.material.color -= new Color(0, 0, 0, 0.08f);

        flashClick = true;
    }

    //Changes colour and waits for specified amount of time to change colour
    private IEnumerator BlankScreenDisappear()
    {
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);

        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        yield return new WaitForSeconds(0.10f);
        rendBlank.material.color -= new Color(0, 0, 0, 0.08f);
        startTreesNMountainMovement = true;
    }
}