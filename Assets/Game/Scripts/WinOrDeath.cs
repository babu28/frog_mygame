﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinOrDeath : MonoBehaviour
{


    public GameObject saveManager;

    // Start is called before the first frame update
    void Start()
    {



    }

    private void Awake()
    {
        saveManager = GameObject.FindGameObjectWithTag("SaveManager");


    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            saveManager.GetComponent<LoadAndSave>().died = true;

    }
}
