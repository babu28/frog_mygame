﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour
{

    [SerializeField]
    float deathTimer = 2.0f;
    void Start()
    {
        Destroy(this.gameObject, deathTimer);
    }

}
